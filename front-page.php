<?php get_header(); ?>

<?php get_template_part( 'template-parts/hero', get_post_type() ); ?>

<main class="container--full relative nf-hack">
        <div class="flex flex-wrap">
            <div class="w-full py-8 lg:py-16 -mt-2">
                <?php while ( have_posts() ): the_post();
                    the_content();
                endwhile; ?>
            </div>
         </div>
</main>
<?php get_template_part( 'template-parts/contact-form')?>
<?php get_footer(); ?>


