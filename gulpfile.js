// Gulp
const gulp = require("gulp");

// CSS related packages
const postcss = require("gulp-postcss");
const sourcemaps = require("gulp-sourcemaps");

// JS related packages
const webpack = require("webpack-stream");

// Other packages, utilities
const browserSync = require("browser-sync").create();
const noop = require("gulp-noop");
const del = require("del");
const rename = require("gulp-rename");
const path = require("path");

const inDevelopment = process.env.NODE_ENV !== "production";

/**
 * Load WPGulp Configuration.
 *
 * TODO: Customize your project in the wpgulp.js file.
 */
const config = require("./wpgulp.config.js");

/**
 * Clean built assets
 */
exports.clean = () => {
    return del(["public"]);
};

/**
 * Task to process CSS files. Takes the source CSS file,
 * passes it to the PostCSS and writes the result
 * to destination folder with sourcemaps.
 */
const css = () => {
    return gulp
        .src(config.css.src)
        .pipe(inDevelopment ? sourcemaps.init() : noop())
        .pipe(postcss(config.css.postcss.plugins))
        .pipe(inDevelopment ? sourcemaps.write(".") : noop())
        .pipe(rename(config.css.out))
        .pipe(gulp.dest(config.css.dest));
};

exports.css = css;

/**
 * Task to process JS files. Takes the source CSS file,
 * passes it to the PostCSS and writes the result
 * to destination folder with sourcemaps.
 */
const js = () => {
    return gulp
        .src(config.scripts.src)
        .pipe(
            webpack({
                mode: inDevelopment ? "development" : "production",
                entry: configureEntries(),
                output: {
                    path: __dirname + "/" + config.webpack.output.path,
                    filename: config.webpack.output.filename,
                },
                devtool: inDevelopment ? "inline-cheap-module-source-map" : "",
                module: {
                    rules: [
                        // Scripts
                        {
                            test: /\.js$/,
                            exclude: /node_modules/,
                            use: {
                                loader: "babel-loader",
                                options: {
                                    presets: [
                                        [
                                            "@babel/preset-env",
                                            {
                                                useBuiltIns: "entry",
                                            },
                                        ],
                                    ],
                                },
                            },
                        },
                    ],
                },
            })
        )
        .pipe(gulp.dest(config.scripts.dest));
};

exports.js = js;

/**
 * Browsersync task
 */
const browsersync = done => {
    browserSync.init({
        proxy: config.browsersync.proxyUrl || config.urls.local,
        open: config.browsersync.browserAutoOpen,
        injectChanges: config.browsersync.injectChanges,
        watchEvents: ["change", "add", "unlink", "addDir", "unlinkDir"],
    });
    done();
};

exports.browsersync = browsersync;

/**
 * Reload browsersync browser instance
 * @param done
 */
const bsReload = done => {
    browserSync.reload();
    done();
};

/**
 * Reload browsersync browser instance
 * @param done
 */
const bsReloadCss = done => {
    browserSync.reload("*.css");
    done();
};

/**
 * Configure & Normalize Webpack entries
 */
const configureEntries = () => {
    const entries = {};

    for (const [key, value] of Object.entries(config.webpack.entries)) {
        entries[key] = path.resolve(__dirname, value);
    }

    return entries;
};

/**
 * Task to watch CSS & JS changes. Runs CSS & JS tasks prehand
 */
exports.watch = gulp.series(
    exports.css,
    exports.js,
    gulp.parallel(browsersync, () => {
        gulp.watch(config.watch.styles, gulp.series(css, bsReloadCss));
        gulp.watch(config.watch.tailwindConfig, gulp.series(css, bsReloadCss));
        gulp.watch(config.watch.scripts, gulp.series(js, bsReload));
        gulp.watch(config.watch.templates, bsReload);
    })
);

exports.default = gulp.parallel(exports.css, exports.js);
