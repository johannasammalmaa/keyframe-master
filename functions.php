<?php

/** Theme setup */
require_once 'inc/functions-setup.php';

/** Shortcodes */
require_once 'inc/functions-appends.php';

/** Load assets */
require_once 'inc/functions-assets.php';

/** Filters */
require_once 'inc/functions-filters.php';

/** Filters */
require_once 'inc/functions-duplicate-posts.php';

/** Remove comments from whole system */
require_once 'inc/remove-commenting.php';

/** Custom Nav_Walker class */
require_once 'inc/Custom_Nav_Walker.php';

/** Custom mime-types */
require_once 'inc/mime-types.php';

/** Helper functions */
require_once 'inc/utils.php';

/** Custom post types */
/*require_once 'inc/custom-post-types.php';*/

/** Custom taxonomies */
require_once 'inc/custom-taxonomies.php';

/** Custom fields */
require_once 'inc/custom-fields.php';

/** Widgets */
require_once 'inc/functions-widgets.php';

/** Shortcodes */
require_once 'inc/functions-shortcodes.php';

/** Custom function */
require_once 'inc/functions-custom.php';

/** Breadcrumbs */
require_once 'inc/breadcrumbs.php';
