<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gutenbergtheme
 */

// WP_Query arguments
// The Query
$query = new WP_Query( [
    'post_type'   => [ 'page' ],
    'order'       => 'ASC',
    'orderby'     => 'menu_order',
    'post_parent' => 0,
] );

$social_media_links = [
    'facebook'  => 'Facebook',
    'instagram' => 'Instagram',
    'linkedin'  => 'LinkedIn',
    'twitter'   => 'Twitter',
    'vimeo' => 'Vimeo',
    'youtube'   => 'YouTube',
];

$company_phone = carbon_get_theme_option( 'crb_company_phone' );
$company_email = carbon_get_theme_option( 'crb_company_email' );
?>

<footer class="pt-12 pb-0 bg-black text-center text-lg text-white">
            <h2 class="mb-2 h6">
                <?= carbon_get_theme_option( 'crb_company_official_name' ) ?>
            </h2>
            <p class="mb2">
                <?php if ( ! empty( $company_phone ) ): ?>
                            <br>
                            <?php $company_phone_strip = str_replace(' ', '', $company_phone);?>
                            p. <a class="text-white" href="tel:<?= $company_phone_strip ?>">
                                <?= $company_phone ?>
                            </a>
                <?php endif; ?>
            </p>
            <address class="not-italic">
                <p class="mb-2">
                    <?= esc_html( carbon_get_theme_option( 'crb_company_address' ) ) ?>
                    <?php if ( ! empty( $company_address ) ): ?>
                        <br>
                            <?= $company_address?>
                    <?php endif; ?>

                    <?php if ( ! empty( $company_email ) ): ?>
                        <br>
                        <a class="text-white" href="mailto:<?= esc_html( antispambot( $company_email ) ) ?>">
                            <?= esc_html( antispambot( $company_email ) ) ?>
                        </a>
                    <?php endif; ?>
                </p>
            </address>

            <ul class="list-reset mb-0 flex flex-wrap justify-center items-stretch">
                <?php foreach ( $social_media_links as $type => $label ): ?>
                    <?php
                    // Get the url from the database
                    $url = carbon_get_theme_option( 'crb_social_url_' . $type );

                    // Skip this service if no url has been entered for it
                    if ( empty( $url ) ) {
                        continue;
                    }
                    ?>
                    <li class="mb-2 px-4">
                        <a href="<?= esc_url( $url ) ?>"
                           class="block"
                           target="_blank"
                           rel="noopener"
                           title="<?= esc_html( $label ) ?>">
                            <img class="h-10 max-h-10 w-auto" src="<?= get_template_directory_uri() ?>/gfx/icons/<?= $type ?>.svg"
                                 alt="<?= esc_html( $label ) ?>" />
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
    </div>

    <div class="bg-black p-4">
        <p class="text-center text-white py-4">
            &copy; <?= date( 'Y' ) ?> <?= carbon_get_theme_option( 'crb_company_official_name' ) ?>
            <?php if ( get_option( 'wp_page_for_privacy_policy' ) ): ?>
                | <a class="text-white" href="<?= get_permalink( get_option( 'wp_page_for_privacy_policy' ) ) ?>">
                    <?= __( 'Tietosuojaseloste' ) ?>
                </a>
            <?php endif; ?>
        </p>
</footer>

<?php wp_footer(); ?>

</body>
</html>
