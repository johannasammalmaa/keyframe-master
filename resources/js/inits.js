import nav from "./navigation";
// import { lory } from "lory.js";

/**
 * Loader for DOM Ready event
 *
 * @param {function} fn
 */
function ready(fn) {
    if (
        document.attachEvent
            ? document.readyState === "complete"
            : document.readyState !== "loading"
    ) {
        fn();
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

/**
 * Call these when DOM is Ready
 */
ready(() => {
    // Call initialization functions here...
    initialize_creamedia_navigation();
    // initialize_logo_carousel();
});

/**
 * Initialize Creamedia Navigation.js
 */
function initialize_creamedia_navigation() {
    nav(document.getElementById("primary-navigation"), {
        desktop_min_height: 501,
        menu_toggles: "[data-menu-toggle]",
    });
}

/**
 * Initialize Lory image carousel
 */
function initialize_logo_carousel() {
    const slider = document.querySelector(".js-carousel");

    if (slider !== null) {
        let logo_carousel = lory(slider, {
            rewind: true,
            enableMouseEvents: true,
            ease: "cubic-bezier(0.455, 0.03, 0.515, 0.955)",
            classNameFrame: "js-carousel-frame",
            classNameSlideContainer: "js-carousel-slides",
            classNamePrevCtrl: "js-carousel-prev",
            classNameNextCtrl: "js-carousel-next",
        });

        setInterval(() => logo_carousel.next(), 2000);
    }
}

//Desktop navigation change while scrolling
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.getElementById("header").classList.remove("bg-transparent");
    document.getElementById("header").classList.add("bg-white");
    document.getElementById("header").classList.remove("py-6");
    document.getElementById("header").classList.add("py-4");
    document.getElementById("Path_1").style.fill="#000";
    document.getElementById("Path_2").style.fill="#000";
    document.getElementById("Path_3").style.fill="#000";
    document.getElementById("Path_4").style.fill="#000";
    document.getElementById("Path_5").style.fill="#000";
    document.getElementById("Path_6").style.fill="#000";
    document.getElementById("Path_7").style.fill="#000";
    document.getElementById("Path_8").style.fill="#000";
    document.getElementById("Path_9").style.fill="#bc319f";
    document.getElementById("Path_10").style.fill="#e71f7f";
    document.getElementById("Path_11").style.fill="#ec7900";
    document.getElementById("Path_11").classList.add("scale");
    document.getElementById("Path_10").classList.add("move1");
    document.getElementById("Path_9").classList.add("move2");
    document.getElementById("Path_31").style.fill="#000";
    document.getElementById("bar1").style.backgroundColor="#000";
    document.getElementById("bar2").style.backgroundColor="#000";
    document.getElementById("bar3").style.backgroundColor="#000";
    var toggleactive = document.getElementsByClassName("menu-toggle--active");
    var o;
    for (o = 0; o < toggleactive.length; o++) {
    var togglebar = toggleactive[0].getElementsByClassName("bar");
    var j;
        for (j = 0; j < togglebar.length; j++) {
            togglebar[j].style.backgroundColor="#fff";
        }
    }
    if (screen.width > 991 && window.innerWidth > 991 || window.matchMedia("(orientation: landscape)").matches) {
        document.getElementById("submenu_indicator-2").style.fill="#000";
        var menuitems = document.getElementsByClassName('menu-item');
        var i;
        for (i = 0; i < menuitems.length; i++) {
            menuitems[i].getElementsByTagName('a')[0].style.color="#000";
            menuitems[i].getElementsByTagName('svg')[0].getElementsByTagName('path')[0].style.fill="#000"; }
            var submenus = document.getElementsByClassName('sub-menu');
            var y;
            for (y = 0; y < submenus.length; y++) {
                var submenuitems = submenus[y].getElementsByClassName('menu-item');
                var n;
                for (n = 0; n < submenuitems.length; n++) {
                    submenuitems[n].getElementsByTagName('a')[0].style.color="#fff";
                }
            }
        var langitems = document.getElementsByClassName('lang-item');
        var j;
        for (j = 0; j < langitems.length; j++) {
            langitems[j].getElementsByTagName('a')[0].style.color="#000";
        }
    }
        
    
  } else {
    document.getElementById("header").classList.remove("bg-white");
    document.getElementById("header").classList.add("bg-transparent");
    document.getElementById("header").classList.remove("py-4");
    document.getElementById("header").classList.add("py-6");
    document.getElementById("Path_1").style.fill="#fff";
    document.getElementById("Path_2").style.fill="#fff";
    document.getElementById("Path_3").style.fill="#fff";
    document.getElementById("Path_4").style.fill="#fff";
    document.getElementById("Path_5").style.fill="#fff";
    document.getElementById("Path_6").style.fill="#fff";
    document.getElementById("Path_7").style.fill="#fff";
    document.getElementById("Path_8").style.fill="#fff";
    document.getElementById("Path_9").style.fill="#fff";
    document.getElementById("Path_10").style.fill="#fff";
    document.getElementById("Path_11").style.fill="#fff";
    document.getElementById("Path_11").classList.remove("scale");
    document.getElementById("Path_10").classList.remove("move1");
    document.getElementById("Path_9").classList.remove("move2");
    document.getElementById("Path_31").style.fill="#fff";
    document.getElementById("bar1").style.backgroundColor="#fff";
    document.getElementById("bar2").style.backgroundColor="#fff";
    document.getElementById("bar3").style.backgroundColor="#fff";
    if (screen.width > 991 && window.innerWidth > 991 || window.matchMedia("(orientation: landscape)").matches) {
        document.getElementById("submenu_indicator-2").style.fill="#fff";
        var menuitems = document.getElementsByClassName('menu-item');
        var i;
        for (i = 0; i < menuitems.length; i++) {
            menuitems[i].getElementsByTagName('a')[0].style.color="#fff"
            menuitems[i].getElementsByTagName('svg')[0].getElementsByTagName('path')[0].style.fill="#fff";
        }
        var submenus = document.getElementsByClassName('sub-menu');
        var y;
        for (y = 0; y < submenus.length; y++) {
            var submenuitems = submenus[y].getElementsByClassName('menu-item');
            var n;
            for (n = 0; n < submenuitems.length; n++) {
                submenuitems[n].getElementsByTagName('a')[0].style.color="#fff";
            }
        }
        var langitems = document.getElementsByClassName('lang-item');
        var j;
        for (j = 0; j < langitems.length; j++) {
            langitems[j].getElementsByTagName('a')[0].style.color="#fff";
        }
    }
  }
}
