/**
 * Creamedia Navigation.js
 *
 * Fork of Aucor Navigation.js
 * @link https://github.com/aucor/aucor-starter
 * -------------------
 *
 * Features:
 * - only adds classes, no show() or hide()
 * - timer for hover exit: better usability
 * - works with tabs: a11y
 * - desktop menu with touch: doubletap
 * - mobile menu with touch
 * - works at least with 3 levels (probably more)
 *
 * @version 1.0.0
 * @author Tommi Finnilä <tommi.finnila@creamedia.fi>
 * @author Aucor
 */

let creamedia_navigation;
creamedia_navigation = function(menu, options) {
    const extend = (defaults, options) => {
        const extended = {};
        let prop;
        for (prop in defaults) {
            if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
                extended[prop] = defaults[prop];
            }
        }
        for (prop in options) {
            if (Object.prototype.hasOwnProperty.call(options, prop)) {
                extended[prop] = options[prop];
            }
        }
        return extended;
    };

    // Default settings
    let defaults = {
        desktop_min_width: 501,
        menu_toggles: ".js-menu-toggle",
    };

    const settings = extend(defaults, options);
    const desktop_min_width = settings.desktop_min_width; // match this to $menu-visible SASS variable
    const menu_toggles = document.querySelectorAll(settings.menu_toggles);
    let screen_w, hover_timer;

    /**
     * Is desktop menu
     *
     * Checks if window is wider than set desktop limit.
     *
     * @return bool is desktop width screen
     */
    function is_desktop_menu() {
        screen_w = Math.max(
            document.documentElement.clientWidth,
            window.innerWidth || 0
        );
        return screen_w >= desktop_min_width;
    }

    /**
     * Hover timer (only for desktop menus)
     *
     * Keeps sub-menu open for a small time when hover has left the element.
     */
    const open_sub_menu = function() {
        if (!is_desktop_menu()) {
            return;
        }

        // clear timer
        clearTimeout(hover_timer);

        // make sure hover_timer did it's thing even if it didn't have time to fire
        // -> close all .sub-menus that don't belong to this DOM tree
        const this_tree_submenus = [];

        // get submenus on tree parents
        let current_parent = this.parentElement;
        while (!current_parent.isEqualNode(menu)) {
            if (current_parent.classList.contains("sub-menu")) {
                this_tree_submenus.push(current_parent);
            }
            current_parent = current_parent.parentElement;
        }

        // get submenus on tree descendants
        const current_descendants = this.querySelectorAll(".sub-menu");
        for (let d = 0; d < current_descendants.length; d++) {
            this_tree_submenus.push(current_descendants[d]);
        }

        // fetch all open submenus
        const all_open_sub_menus = menu.querySelectorAll(".open");
        for (let j = 0; j < all_open_sub_menus.length; j++) {
            // close the submenu only if not in current tree
            if (this_tree_submenus.indexOf(all_open_sub_menus[j]) === -1) {
                all_open_sub_menus[j].classList.remove("open");
            }
        }

        // open child sub-menu
        if (this.querySelector(".sub-menu")) {
            this.querySelector(".sub-menu").classList.add("open");
        }
    };

    /**
     * Close menu when hover timer has ended (only for desktop menus)
     *
     * Triggers when mouse leaves menu element.
     */
    const close_sub_menu = function() {
        const t = this;
        // create timeout that let's the cursor get outside of menu for a moment
        if (is_desktop_menu()) {
            hover_timer = setTimeout(() => {
                let parent = t.parentElement;
                while (!parent.isEqualNode(menu)) {
                    parent.classList.remove("open");
                    parent = parent.parentElement;
                }
                if (t.querySelector(".open")) {
                    t.querySelector(".open").classList.remove("open");
                }
            }, 750);
        }
    };

    const open_submenu_with_click = e => {
        /** @type {null|HTMLElement} */
        let li = null;

        /** @type {HTMLElement} */
        let parent = e.target.parentElement;

        while (!parent.isEqualNode(menu)) {
            if (parent.classList.contains("menu-item")) {
                li = parent;
                break;
            }
            parent = parent.parentElement;
        }

        if (li instanceof HTMLElement) {
            // toggle .open class to child .sub-menu
            li.querySelector(".sub-menu").classList.toggle("open");

            // toggle .active class to this <li>
            if (!is_desktop_menu()) {
                li.classList.toggle("active");
            }
        }

        // don't trigger parent(s)
        e.stopPropagation();
    };

    const items_with_children = menu.querySelectorAll(
        ".menu-item-has-children"
    );
    for (let i = 0; i < items_with_children.length; i++) {
        const item = items_with_children[i];
        item.addEventListener("mouseover", open_sub_menu);
        item.addEventListener("mouseleave", close_sub_menu);

        const caret = item.querySelector(".js-menu-caret");
        if (caret) {
            /* Open sub-menu with click to <button>
      ----------------------------------------------- */
            caret.addEventListener("click", open_submenu_with_click);
        }
    }

    /* Keyboard (tab)
  ----------------------------------------------- */
    const on_link_focus = e => {
        // open sub-menu below
        const submenu_below = e.target.parentElement.querySelector(".sub-menu");
        if (submenu_below) {
            submenu_below.classList.add("open");
        }

        // open all sub-menus above
        let parent = e.target.parentElement;
        while (!parent.isEqualNode(menu)) {
            if (parent.classList.contains("sub-menu")) {
                parent.classList.add("open");
            }
            parent = parent.parentElement;
        }
    };

    const on_link_blur = e => {
        // close sub-menu below
        const submenu_below = e.target.parentElement.querySelector(".sub-menu");
        if (submenu_below) {
            submenu_below.classList.remove("open");
        }

        // close all sub-menus above
        let parent = e.target.parentElement;
        while (!parent.isEqualNode(menu)) {
            if (parent.classList.contains("sub-menu")) {
                parent.classList.remove("open");
            }
            parent = parent.parentElement;
        }
    };

    const links = menu.querySelectorAll("a");
    for (let k = 0; k < links.length; k++) {
        const link = links[k];

        link.addEventListener("focus", on_link_focus);
        link.addEventListener("blur", on_link_blur);
    }

    /* Toggle menu (hamburger)
  ----------------------------------------------- */

    for (let j = 0; j < menu_toggles.length; j++) {
        menu_toggles[j].addEventListener("click", () => {
            if (menu_toggles[j].classList.contains("menu-toggle--active")) {
                // remove .active class from hamburger icon
                menu_toggles[j].classList.remove("menu-toggle--active");
                menu_toggles[j].setAttribute("aria-expanded", "false");

                // focus out of the menu
                if (typeof Event === "function") {
                    menu_toggles[j].dispatchEvent(new Event("focus"));
                }
            } else {
                // .active class to hamburger icon
                menu_toggles[j].classList.add("menu-toggle--active");
                menu_toggles[j].setAttribute("aria-expanded", "true");
            }

            if (menu.classList.contains("active")) {
                // remove .active class to menu container
                menu.classList.remove("active");
            } else {
                // .active class to menu container
                menu.classList.add("active");
            }
        });
    }

    /* Empty links "#": open sub-menu
  ----------------------------------------------- */

    //  $menu.find('a[href="#"]').click(function(e) {
    //
    //    // don't go to "#"
    //    e.preventDefault();
    //
    //    // do the same stuff as clicking to .menu-item-has-children
    //    $(this).parent('.menu-item-has-children').trigger('click');
    //
    //  });

    /* Touch + desktop menu: doubletap
  ----------------------------------------------- */

    let touchStartFn;
    let maybeCloseMenuFn;

    if ("ontouchstart" in window) {
        const findAndRemoveClass = (container, className) => {
            const elements = container.querySelectorAll("." + className);
            for (let e = 0; e < elements.length; e++) {
                elements[e].classList.remove(className);
            }
        };

        const targetInsideMenu = elem => {
            let isInsideMenu = false;
            while ((elem = elem.parentElement) !== null) {
                if (elem.nodeType !== Node.ELEMENT_NODE) {
                    continue;
                }
                if (elem.isEqualNode(menu)) {
                    isInsideMenu = true;
                }
            }
            return isInsideMenu;
        };

        // maybe close menu after it has been opened by tap
        maybeCloseMenuFn = e => {
            // if the target of the tap isn't menu nor a descendant of menu
            if (
                menu !== e.target &&
                !targetInsideMenu(e.target) &&
                is_desktop_menu()
            ) {
                // reset menu state to default
                findAndRemoveClass(menu, "open");
                findAndRemoveClass(menu, "tapped");
                findAndRemoveClass(menu, "active");
            }

            // remove this event listener
            document.removeEventListener(
                "ontouchstart",
                maybeCloseMenuFn,
                false
            );
        };

        touchStartFn = function(e) {
            // only fire on desktop menu
            if (!is_desktop_menu()) {
                return false;
            }

            const current_list_item = this.parentElement;
            let current_parent;

            if (!current_list_item.classList.contains("tapped")) {
                // first tap: don't go to <a> yet
                e.preventDefault();

                // remove .tapped class to <li> that don't belong to this DOM tree
                const this_parents_li = [];
                current_parent = current_list_item;
                while (!current_parent.isEqualNode(menu)) {
                    if (current_parent.classList.contains("tapped")) {
                        this_parents_li.push(current_parent);
                    }
                    current_parent = current_parent.parentElement;
                }

                const all_tapped = menu.querySelectorAll(".tapped");
                for (let j = 0; j < all_tapped.length; j++) {
                    // Close the submenu only if not in current tree
                    if (this_parents_li.indexOf(all_tapped[j]) === -1) {
                        all_tapped[j].classList.remove("tapped");
                    }
                }

                // add .tapped class to <li> element
                current_list_item.classList.add("tapped");

                // close all .sub-menus that don't belong to this DOM tree
                const this_parents_submenu = [];
                current_parent = current_list_item;
                while (!current_parent.isEqualNode(menu)) {
                    if (current_parent.classList.contains("open")) {
                        this_parents_submenu.push(current_parent);
                    }
                    current_parent = current_parent.parentElement;
                }

                const all_open_submenus = menu.querySelectorAll(".open");
                for (let t = 0; t < all_open_submenus.length; t++) {
                    // Close the submenu only if not in current tree
                    if (
                        this_parents_submenu.indexOf(all_open_submenus[t]) ===
                        -1
                    ) {
                        all_open_submenus[t].classList.remove("open");
                    }
                }

                // open .sub-menu below
                if (current_list_item.querySelector(".sub-menu")) {
                    current_list_item
                        .querySelector(".sub-menu")
                        .classList.add("open");
                }

                // open all .sub-menus above
                current_parent = this.parentElement;
                while (!current_parent.isEqualNode(menu)) {
                    if (current_parent.classList.contains("sub-menu")) {
                        current_parent.classList.add("open");
                    }
                    current_parent = current_parent.parentElement;
                }

                // add EventListener to second click
                document.addEventListener(
                    "touchstart",
                    maybeCloseMenuFn,
                    false
                );
            } else {
                // second tap: go to <a>

                // remove .tapped from current <li>
                current_list_item.classList.remove("tapped");

                // close .sub-menus
                findAndRemoveClass(menu, "open");
            }
        };

        // add eventlisteners for each <a> with a sub-menu
        const parent_links = menu.querySelectorAll(
            ".menu-item-has-children > a"
        );
        for (let p = 0; p < parent_links.length; p++) {
            parent_links[p].addEventListener("touchstart", touchStartFn, false);
        }
    }

    // make the call chainable
    return this;
};

export default creamedia_navigation;
