let path = require("path");

/**
 * WP Gulp config
 */

let config = {
    // Project
    urls: {
        local: path.basename(process.cwd().split('/wp-content')[0]) + ".test", // Set local url dynamically
    },

    // Files/globs to watch for changes
    watch: {
        styles: ["resources/postcss/**/*.pcss"],
        scripts: ["resources/js/**/*.js"],
        templates: ["./**/*.php"],
        tailwindConfig: ["tailwind.js"],
    },

    // CSS
    css: {
        src: "./resources/postcss/main.pcss",
        out: "main.css",
        dest: "./public",
        // PostCSS configuration
        postcss: {
            devPlugins: [
                require("postcss-import"),
                require("tailwindcss")("tailwind.js"),
                require("postcss-nested"),
                require("autoprefixer"),
            ],
            prodPlugins: [
                require("@fullhuman/postcss-purgecss")({
                    content: ["**/*.php"],
                    css: ["public/site.css"],
                    whitelistPatterns: 
                    [/error/,
                     /tpl-/,
                     /site-navigation/,
                     /menu-item/,
                     /custom-languages/,
                     /lang-item/,
                     /bg-white/,
                     /hero/,
                     /wp-block/,
                     /alignfull/,
                     /alignwide/,
                     /nf-/,
                     /has-pink-/,
                     /has-purple/,
                     /move1/,
                     /move2/,
                     /scale/,
                     /logo-path/,
                     /logot/,
                    ],
                    extractors: [
                        {
                            extractor: tailwindCssExtractor(),
                            extensions: ["html", "js", "php", "css", "svg"], // Specify the file extensions to include when scanning for class names
                        },
                    ],
                }),
                require("cssnano"),
            ],
        },
    },

    // Javascript
    scripts: {
        src: "./resources/js/main.js",
        out: "main.js",
        dest: "./public",
    },

    // Webpack
    webpack: {
        entries: {
            main: "./resources/js/main.js",
        },
        output: {
            path: "public",
            filename: "js/[name].js",
        },
    },

    // Browser Sync
    browsersync: {
        proxyUrl: null,
        browserAutoOpen: false,
        injectChanges: true,
    },
};

if (process.env.NODE_ENV === "production") {
    config.css.postcss.plugins = [
        ...config.css.postcss.devPlugins,
        ...config.css.postcss.prodPlugins,
    ];
} else {
    config.css.postcss.plugins = config.css.postcss.devPlugins;
}

/**
 * Return a custom PurgeCSS extractor for Tailwind that allows special characters in class names
 *
 * @returns object
 */
function tailwindCssExtractor() {
    return class {
        // noinspection JSUnusedGlobalSymbols
        static extract(content) {
            return content.match(/[A-z0-9-:\/]+/g) || [];
        }
    };
}

module.exports = config;
