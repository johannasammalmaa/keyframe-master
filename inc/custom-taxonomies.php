<?php

/**
 * Add 'Genres' hiearchial taxonomy
 */
function creamedia_starter_register_genres_taxonomy() {
    $labels = [
        'name'              => _x( 'Genres', 'taxonomy general name', 'creamedia-starter' ),
        'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'creamedia-starter' ),
        'search_items'      => __( 'Search Genres', 'creamedia-starter' ),
        'all_items'         => __( 'All Genres', 'creamedia-starter' ),
        'parent_item'       => __( 'Parent Genre', 'creamedia-starter' ),
        'parent_item_colon' => __( 'Parent Genre:', 'creamedia-starter' ),
        'edit_item'         => __( 'Edit Genre', 'creamedia-starter' ),
        'update_item'       => __( 'Update Genre', 'creamedia-starter' ),
        'add_new_item'      => __( 'Add New Genre', 'creamedia-starter' ),
        'new_item_name'     => __( 'New Genre Name', 'creamedia-starter' ),
        'menu_name'         => __( 'Genres', 'creamedia-starter' ),
    ];

    register_taxonomy( 'genres', [ 'cm_books' ], [
        'labels'                => $labels,
        'hierarchical'          => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        'show_in_quick_edit'    => false,
        'show_admin_column'     => true,
        'rewrite'               => [
            'slug'       => 'genre',
            'with_front' => true,
        ],
        'query_var'             => true,
        'show_in_rest'          => true,
        'rest_base'             => 'genres',
        'rest_controller_class' => 'WP_REST_Terms_Controller',
    ] );
}
add_action( 'init', 'creamedia_starter_register_genres_taxonomy', 0 );

/**
 * Add 'Genres' hiearchial taxonomy
 */
function creamedia_starter_register_topics_taxonomy() {
    $labels = [
        'name'              => _x( 'Topics', 'taxonomy general name', 'creamedia-starter' ),
        'singular_name'     => _x( 'Topic', 'taxonomy singular name', 'creamedia-starter' ),
        'search_items'      => __( 'Search Topics', 'creamedia-starter' ),
        'all_items'         => __( 'All Topics', 'creamedia-starter' ),
        'parent_item'       => __( 'Parent Topic', 'creamedia-starter' ),
        'parent_item_colon' => __( 'Parent Topic:', 'creamedia-starter' ),
        'edit_item'         => __( 'Edit Topic', 'creamedia-starter' ),
        'update_item'       => __( 'Update Topic', 'creamedia-starter' ),
        'add_new_item'      => __( 'Add New Topic', 'creamedia-starter' ),
        'new_item_name'     => __( 'New Topic Name', 'creamedia-starter' ),
        'menu_name'         => __( 'Topics', 'creamedia-starter' ),
    ];

    register_taxonomy( 'topics', [ 'cm_books' ], [
        'labels'                => $labels,
        'hierarchical'          => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        'show_in_quick_edit'    => false,
        'show_admin_column'     => true,
        'rewrite'               => [
            'slug'       => 'topic',
            'with_front' => true,
        ],
        'query_var'             => true,
        'show_in_rest'          => true,
        'rest_base'             => 'topics',
        'rest_controller_class' => 'WP_REST_Terms_Controller',
    ] );
}
add_action( 'init', 'creamedia_starter_register_topics_taxonomy', 0 );
