<?php
/**
 * Actions to append content to different sections
 */

/**
 * Append to <head>
 *
 * @return void
 */
function creamedia_starter_append_to_head() {
}
add_action( 'wp_head', 'creamedia_starter_append_to_head' );

/**
 * Append to footer
 *
 * @return void
 */
function creamedia_starter_append_to_footer() {
}
add_action( 'wp_footer', 'creamedia_starter_append_to_footer' );

/**
 * Favicons
 *
 * Add favicons' <link> and <meta> tags here
 *
 * @return void
 */
function creamedia_starter_favicons() {
}
add_action( 'wp_head', 'creamedia_starter_favicons' );
add_action( 'admin_head', 'creamedia_starter_favicons' );
add_action( 'login_head', 'creamedia_starter_favicons' );
