<?php
/**
 * Functions, hooks and filter for styles and scripts
 */

/**
 * Enqueue theme related scripts and styles
 *
 * @return void
 */
function creamedia_starter_theme_scripts() {
    wp_enqueue_style(
        'creamedia-starter-styles',
        get_template_directory_uri() . '/public/main.css',
        [],
        filemtime( get_template_directory() . '/public/main.css' )
    );

    wp_enqueue_script(
        'creamedia-starter-scripts',
        get_template_directory_uri() . '/public/js/main.js',
        [],
        filemtime( get_template_directory() . '/public/js/main.js' ),
        true
    );
}
add_action( 'wp_enqueue_scripts', 'creamedia_starter_theme_scripts' );

/**
 * Append to <head>
 *
 * @return void
 */
function creamedia_starter_detect_javascript() {
    echo "<script>(function(d){d.className = d.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'creamedia_starter_detect_javascript' );
