<?php
/**
 * Theme setup functions
 */

/**
 * Register theme features
 *
 * @return void
 */
function creamedia_starter_features() {
    /**
     * Enable support for Post Thumbnails (Featured images) on selected pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails
     */
    add_theme_support( 'post-thumbnails' );

    // Add theme support for HTML5 Semantic Markup
    add_theme_support( 'html5', [
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ] );

    // Add theme support for document Title tag
    add_theme_support( 'title-tag' );

    // Add selective refresh for widgets.
    //add_theme_support( 'customize-selective-refresh-widgets' );

    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support( 'custom-logo', [
        'height'      => 250,
        'width'       => 250,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => [ 'site-title', 'site-description' ],
    ] );

    // Adding support for core block visual styles.
    add_theme_support( 'wp-block-styles' );

    // Add support for full and wide align images.
    add_theme_support( 'align-wide' );

    // Add support for responsive embeds.
    add_theme_support( 'responsive-embeds' );

    // Scrub some crap off
    remove_action( 'wp_head', 'wp_generator' ); // WordPress version information
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); // Emoji scripts
    remove_action( 'wp_print_styles', 'print_emoji_styles' ); // Emoji styles
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 ); // oEmbed Discovery Links
    remove_action( 'wp_head', 'wlwmanifest_link' ); // WLW Manifest
    remove_action( 'wp_head', 'rsd_link' ); // RSD Link
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 ); // Shortlink
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 ); // Previous/next post links
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 ); // wp-json
    remove_action( 'wp_head', 'locale_stylesheet' );
    remove_action( 'wp_head', 'wp_shortlink_wp_head' );

    // Remove build-in Gutenberg styles
    // remove_action( 'wp_enqueue_scripts', 'gutenberg_register_scripts_and_styles', 5 );
    // remove_action( 'admin_enqueue_scripts', 'gutenberg_register_scripts_and_styles', 5 );
}
add_action( 'after_setup_theme', 'creamedia_starter_features' );

/**
 * Add 'Featured image' sizes
 *
 * @return void
 */
function creamedia_starter_add_image_sizes() {
    add_image_size( 'hero-large', 1600, 1200, true ); // width, height, crop
    add_image_size( 'hero-smaller', 1200, 400, true );
}
add_action( 'after_setup_theme', 'creamedia_starter_add_image_sizes' );

/**
 * Custom image sizes for theme
 *
 * @param $sizes
 *
 * @return array
 */
function creamedia_starter_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, [
        'hero-large' => __( 'Hero large', 'creamedia-starter' ),
    ] );
}
//add_filter( 'image_size_names_choose', 'creamedia_starter_custom_image_sizes' );

/**
 * Register menus
 *
 * @link   https://developer.wordpress.org/reference/functions/register_nav_menus/
 * @return void
 */
function creamedia_starter_register_menus() {
    register_nav_menu( 'header-menu', __( 'Header Menu', 'creamedia-starter' ) );
}
add_action( 'init', 'creamedia_starter_register_menus' );

/**
 * Add excerpt to page
 *
 * @return void
 */
add_action( 'init', function () {
    add_post_type_support( 'page', 'excerpt' );
} );

/**
 * Register translatable strings with Polylang and
 * use them in templates e.g. <?= pll__('Custom string...') ?>
 *
 * @link https://polylang.pro/doc/function-reference/#pll_register_string
 * @see https://polylang.pro/doc/function-reference/#pll__
 */
function creamedia_starter_register_pll_strings() {
    if ( function_exists( 'pll_register_string' ) ) {
        pll_register_string( 'creamedia-starter', 'Custom string...' );
    }
}
add_action( 'init', 'creamedia_starter_register_pll_strings' );
