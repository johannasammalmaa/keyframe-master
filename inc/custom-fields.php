<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Make Theme Options accessible to everyone
 * Role restrictions are specified after Container::make
 *
 * @link https://github.com/htmlburger/carbon-fields/issues/326#issuecomment-326236771
 */
add_filter( 'carbon_fields_theme_options_container_admin_only_access', '__return_false' );

/**
 * Add Theme Options
 */
function crb_attach_theme_options() {
    // Sivuston perustiedot
    $site_information =
        Container::make( 'theme_options', __( 'Site information', 'creamedia-starter' ) )
                 ->where( 'current_user_capability', '=', 'edit_pages' )
                 ->set_page_menu_position( 80 )
                 ->set_icon( 'dashicons-dashboard' )
                 ->add_tab( __( 'Company', 'creamedia-starter' ), [
                     Field::make( 'text', 'crb_company_marketing_name', __( 'Marketing name', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_company_official_name', __( 'Official name', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_company_official_url', __( 'Link to official website', 'creamedia-starter' ) ),
                     Field::make( 'textarea', 'crb_company_address', __( 'Address', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_company_phone', __( 'Phone number', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_company_email', __( 'Email address', 'creamedia-starter' ) ),
                     Field::make( 'image', 'crb_company_default_image', __( 'Default image', 'creamedia-starter' ) ),

                 ] )
                 ->add_tab( __( 'Social media', 'creamedia-starter' ), [
                     Field::make( 'text', 'crb_social_url_facebook', 'Facebook URL' )
                          ->set_help_text( __( 'Enter your Facebook page url', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_social_url_instagram', 'Instagram URL' )
                          ->set_help_text( __( 'Enter your Instagram page url', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_social_url_linkedin', 'LinkedIn URL' )
                          ->set_help_text( __( 'Enter your Linked page url', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_social_url_twitter', 'Twitter URL' )
                          ->set_help_text( __( 'Enter your Twitter page url', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_social_url_vimeo', 'Vimeo URL' )
                          ->set_help_text( __( 'Enter your Vimeo page url', 'creamedia-starter' ) ),
                     Field::make( 'text', 'crb_social_url_youtube', 'YouTube URL' )
                          ->set_help_text( __( 'Enter your YouTube page url', 'creamedia-starter' ) ),
                 ] )
                 ->add_tab( __( '404 page', 'creamedia-starter' ), [
                    Field::make( 'text', 'crb_404_heading', __( 'Heading', 'creamedia-starter' ) ),
                    Field::make( 'textarea', 'crb_404_text', __( 'Text', 'creamedia-starter' ) ),
                    
                    
                ] );

    Container::make( 'theme_options', __( 'Embed code', 'creamedia-starter' ) )
             ->where( 'current_user_capability', '=', 'edit_pages' )
             ->set_page_parent( $site_information )
             ->set_page_menu_title( __( 'Embed code', 'creamedia-starter' ) )
             ->add_fields( [
                 Field::make( 'header_scripts', 'crb_header_script' ),
                 Field::make( 'footer_scripts', 'crb_footer_script' ),
             ] );
}
add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );


/**
 * Custom Gutenberg blocks
 */
/*function crb_attach_custom_blocks() {
    Container::make( 'block', 'Test block' )
             ->add_fields( [
                 Field::make( 'text', 'name', 'Nimi' ),
                 Field::make( 'rich_text', 'description', 'Kuvaus' ),
                 Field::make( 'image', 'image', 'Kuva' ),
             ] )
             ->set_render_callback( function ( $args ) {
                 include get_template_directory() . '/template-parts/custom-fields/test-block.php';
             } );
}
add_action( 'carbon_fields_register_fields', 'crb_attach_custom_blocks' );*/

/**
 * Custom Fields for Menu 
 */

function crp_add_custom_fields_menu() {
Container::make( 'nav_menu_item', __('Menu Settings' ))
    ->add_fields( array(
        Field::make( 'image', 'menu_image', __( 'Image', 'creamedia-starter' ) )
        ->set_value_type( 'url' )
        ->set_width( 200 )
    ));
}
add_action( 'carbon_fields_register_fields', 'crp_add_custom_fields_menu' );

/**
 * Custom Fields for all pages
 */
function crb_add_custom_fields_all_pages() {
    /**
     * Hero text
     */
    Container::make( 'post_meta', __( 'Hero', 'creamedia-starter' ) )
            ->where( 'post_type', '=', 'page' )
             ->add_fields( [
                 Field::make( 'text', 'hero_heading', __( 'Hero overlay heading', 'creamedia-starter' ) ),
                 Field::make( 'text', 'hero_text', __( 'Hero overlay text', 'creamedia-starter' ) ),
                 Field::make( 'text', 'hero_video', __( 'Hero video ID', 'creamedia-starter' ) )
             ] );

    /**
     * Page highlights
     */
    /*Container::make( 'post_meta', __( 'Page highlights', 'creamedia-starter' ) )
            ->where( 'post_type', '=', 'page' )
             ->add_fields( [
                 Field::make( 'complex', 'crb_page_highlights', __( 'Selected pages', 'creamedia-starter' ) )
                      ->set_layout( 'tabbed-horizontal' )
                      ->add_fields( [
                          Field::make( 'text', 'title' . crb_get_i18n_suffix(), __( 'Title', 'creamedia-starter' ) ),
                          Field::make( 'image', 'image', __( 'Image', 'creamedia-starter' ) )->set_width( 50 ),
                          Field::make( 'association', 'association', __( 'Linked page', 'creamedia-starter' ) )
                               ->set_min( 1 )
                               ->set_max( 1 )
                               ->set_width( 50 )
                               ->set_types( [
                                   [
                                       'type'      => 'post',
                                       'post_type' => 'page',
                                   ],
                                   [
                                       'type'      => 'post',
                                       'post_type' => 'post',
                                   ],
                               ] ),
                      ] )
                      ->set_header_template( '<%- title' . crb_get_i18n_suffix() . ' %>' ),
             ] );*/
                                    
    /**
     * Contact Form Settings
     */
    Container::make( 'post_meta', __( 'Contact Form Settings', 'creamedia-starter' ) )
        ->where( 'post_type', '=', 'page' )
        ->add_fields( [
            Field::make( 'checkbox', 'use_form', 'Show Default Contact Form' ),
            Field::make( 'complex', 'crb_contact_form', __( 'Contact Form Settings', 'creamedia-starter' ) )
            ->set_min(1)
            ->set_max(1)
            ->set_conditional_logic( array(
                array(
                    'field' => 'use_form',
                    'value' => true,
                )
            )) 
            ->add_fields( array(
                Field::make( 'radio', 'bg', 'Select Background Color' )
                ->add_options( array(
                    'white' => 'White',
                    'black' => 'Black',
                    'pink' => 'Pink',
                ) ),
                Field::make( 'text', 'ninja_shortcode', __( 'Ninja Form ID', 'creamedia-starter' ) ),
                Field::make( 'text', 'form_title', __( 'Form Title', 'creamedia-starter' ) ),
                Field::make( 'text', 'slogan', __( 'Slogan', 'creamedia-starter' ) ),
                Field::make( 'checkbox', 'use_image', 'Show Image' ),
                Field::make( 'image', 'form_image', 'Photo' )
                ->set_value_type( 'url' )
                    ->set_conditional_logic( array(
                        array(
                            'field' => 'use_image',
                            'value' => true,
                        )
                    ) )
            ) )
        ] );
}
add_action( 'carbon_fields_register_fields', 'crb_add_custom_fields_all_pages' );

/**
 * Load Carbon Fields
 */
function crb_load() {
    /** @noinspection PhpIncludeInspection */
    require_once get_template_directory() . '/vendor/autoload.php';
    \Carbon_Fields\Carbon_Fields::boot();
}
add_action( 'after_setup_theme', 'crb_load' );
