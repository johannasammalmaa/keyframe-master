<?php
/**
 * Theme shortcodes
 */

/**
 * List child pages
 *
 * @return string
 */
function creamedia_starter_list_childpages() {
    global $post;

    if ( $post->post_parent && is_page() ) {
        $childpages = wp_list_pages( [
            'sort_column' => 'menu_order',
            'title_li'    => '',
            'child_of'    => $post->post_parent,
            'echo'        => false,
        ] );
    } else {
        $childpages = wp_list_pages( [
            'sort_column' => 'menu_order',
            'title_li'    => '',
            'child_of'    => $post->ID,
            'echo'        => false,
        ] );
    }

    if ( $childpages ) {
        return '<ul>' . $childpages . '</ul>';
    }

    return '';
}
add_shortcode( 'creamedia_starter_list_childpages', 'creamedia_starter_list_childpages' );
