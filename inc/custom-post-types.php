<?php

/**
 * Custom post type: Books
 */
function creamedia_starter_cpt_books() {
    $labels = [
        'name'          => __( 'Books', 'creamedia-starter' ),
        'singular_name' => __( 'Book', 'creamedia-starter' ),
        'menu_name'     => __( 'Books', 'creamedia-starter' ),
        'all_items'     => __( 'Books', 'creamedia-starter' ),
    ];

    register_post_type( 'cm_books', [
        'labels'                => $labels,
        'description'           => __( 'Books', 'creamedia-starter' ),
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'tags', 'revisions', 'custom-fields', 'page-attributes', 'templates' ),
        'taxonomies' => array('category', 'post_tag') ,
        'public'                => true,
        'hierarchical'          => false,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        'show_in_admin_bar'     => true,
        'delete_with_user'      => false,
        'show_in_rest'          => true,
        'rest_base'             => 'books',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'has_archive'           => 'books',
        'exclude_from_search'   => false,
        'capability_type'       => 'post',
        'map_meta_cap'          => true,
        'rewrite'               => [
            'slug'       => 'books',
            'with_front' => false,
        ],
        'query_var'             => true,
        'menu_icon'             => 'dashicons-book-alt',
    ] );
}
add_action( 'init', 'creamedia_starter_cpt_books', 0 );
