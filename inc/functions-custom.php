<?php
/**
 * Custom functions
 */

/**
 * Custom implementation of how to create clean custom menu
 *
 * @link https://digwp.com/2011/11/html-formatting-custom-menus
 */
function clean_custom_menus() {
    $menu_name = 'header-menu'; // specify custom menu slug
    if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
        $menu       = wp_get_nav_menu_object( $locations[ $menu_name ] );
        $menu_items = wp_get_nav_menu_items( $menu->term_id );
        $menu_items = array_filter( $menu_items, function ( $item ) {
            /** @var \WP_Post $item */
            return $item->post_parent === 0;
        } );

        $menu_list = '<nav>';
        $menu_list .= '<ul class="list-reset flex mb-0">';

        foreach ( (array) $menu_items as $key => $menu_item ) {
            $title     = $menu_item->title;
            $url       = $menu_item->url;
            $menu_list .= '<li class="mx-2"><a class="text-white" href="' . $url . '">' . $title . '</a></li>';
        }

        $menu_list .= '</ul>';
        $menu_list .= '</nav>';
    } else {
        $menu_list = '<!-- no list defined -->';
    }

    echo $menu_list;
}

/**
 * Return current language code
 *
 * @return string
 */
function crb_get_i18n_suffix() {
    if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE !== 'all' ) {
        return '_' . ICL_LANGUAGE_CODE;
    }

    if ( function_exists( 'pll_default_language' ) ) {
        return '_' . pll_default_language();
    }

    return '';
}

/**
 * Get localized Carbon Field from Theme Options
 *
 * @param $option_name
 *
 * @return mixed
 */
function crb_get_i18n_theme_option( $option_name ) {
    $suffix = crb_get_i18n_suffix();

    return carbon_get_theme_option( $option_name . $suffix );
}

/**
 * Show Polylang Languages with Custom Markup
 *
 * @param string $class Add custom class to the languages container
 *
 * @return string
 */
function creamedia_starter_polylang_languages( $class = '' ) {
    if ( ! function_exists( 'pll_the_languages' ) ) {
        return '';
    }

    $languages = pll_the_languages( [
        'display_names_as'       => 'slug',
        'hide_if_no_translation' => 1,
        'raw'                    => true,
    ] );

    if ( empty( $languages ) ) {
        return '';
    }

    $output = '<div class="languages' . ( $class ?: '' ) . '">';

    foreach ( $languages as $language ) {
        // $id             = $language['id'];
        $slug           = $language['slug'];
        $url            = $language['url'];
        $current        = $language['current_lang'] ? 'languages__item--current' : '';
        $no_translation = $language['no_translation'];

        if ( ! $no_translation ) {
            if ( $current ) {
                $output .= "<span class='languages__item {$current}'>$slug</span>";
            } else {
                $output .= "<a href='{$url}' class='languages__item {$current}'>{$slug}</a>";
            }
        }
    }
    $output .= '</div>';

    return $output;
}

/* Add categories to pages */

/*function add_taxonomies_to_pages() {
    register_taxonomy_for_object_type( 'post_tag', 'page' );
    register_taxonomy_for_object_type( 'category', 'page' );
    }
   add_action( 'init', 'add_taxonomies_to_pages' );*/

/* Setting Gutenberg Color Palette*/

function keyframe_setup_theme_supported_features() {
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'purple', 'themeLangDomain' ),
            'slug' => 'purple',
            'color' => '#bc319f',
        ),
        array(
            'name' => __( 'pink', 'themeLangDomain' ),
            'slug' => 'pink',
            'color' => '#e81e7f',
        ),
        array(
            'name' => __( 'orange', 'themeLangDomain' ),
            'slug' => 'orange',
            'color' => '#ec7900',
        ),
        array(
            'name' => __( 'black', 'themeLangDomain' ),
            'slug' => 'black',
            'color' => '#000',
        ),
        array(
            'name' => __( 'white', 'themeLangDomain' ),
            'slug' => 'white',
            'color' => '#fff',
        ),
    ) );
    add_theme_support( 'disable-custom-colors' );
    add_theme_support( 'editor-styles' );
    add_theme_support( 'dark-editor-style' );
}
add_action( 'after_setup_theme', 'keyframe_setup_theme_supported_features');

// Adding a custom settings to the menu links
add_filter( 'nav_menu_link_attributes', 'crb_nav_menu_link_attributes', 10, 4 );
function crb_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
    $crb_bg = carbon_get_nav_menu_item_meta( $item->ID, 'menu_image' );
    $atts['style'] = ! empty( $crb_bg) ? 'background-image:url('. $crb_bg . ')' : '';
    return $atts;
}


// Adding description to the menu links
add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );
function prefix_nav_description( $item_output, $item, $depth, $args ) {
    if ( !empty( $item->description ) ) {
        $item_output = str_replace( $args->link_after . '</a>', '<p class="menu-item-description">' . $item->description . '</p>' . $args->link_after . '</a>', $item_output );
    }
 
    return $item_output;
}

//Adding a custom button to the menu links
add_filter( 'walker_nav_menu_start_el', 'crea_maxmenu_button', 10, 4 );
function crea_maxmenu_button( $item_output, $item, $depth, $args ) {
    $lang = pll_current_language();
    if ($lang == "fi") { $txt = "Lue lisää"; }
    else { $txt = "Read more";}
    
    if ( !empty( $item->description ) ) {
        $item_output = str_replace( $args->link_after . '</a>', '<button class="max-menu-btn">'.$txt.
        '<svg id="btn_icon" data-name="btn icon" xmlns="http://www.w3.org/2000/svg" width="12.962" height="16.321" viewBox="0 0 12.962 16.321">
          <path id="submenu_indicator" data-name="submenu indicator" d="M2.058,4.68H0c.026-.043.055-.087.084-.128L2.709.809A1.7,1.7,0,0,1,4.081,0,1.7,1.7,0,0,1,5.454.809L8.079,4.551c.029.041.083.127.083.127H6.1l-1.683-2.4a.416.416,0,0,0-.338-.2H4.077a.423.423,0,0,0-.338.2l-1.681,2.4Z" transform="translate(8.962 4.163) rotate(92)"/>
        </svg>
        </button>' . $args->link_after . '</a>', $item_output );
    }
 
    return $item_output;
}