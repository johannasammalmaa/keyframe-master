<?php

/**
 * Class Breadcrumbs
 */
class Breadcrumbs {
    /**
     * @var array
     */
    protected $trail;
    /**
     * @var array
     */
    protected $menu_map;

    public function __construct() {
        $this->trail = [];
        $this->menu_map = [];

        foreach ( wp_get_nav_menus() as $menu ) {
            foreach ( wp_get_nav_menu_items( $menu ) as $menu_item ) {
                $object_type            = $menu_item->object;
                $object_id              = $menu_item->object_id;
                $key                    = "{$object_type}{$object_id}";
                $this->menu_map[ $key ] = $menu_item;
            }
        }
    }

    /**
     * Instantiate self
     *
     * @return Breadcrumbs
     */
    public static function new(): Breadcrumbs {
        return new self();
    }

    /**
     * Return the trail attribute
     *
     * @return array
     */
    public function get_trail(): array {
        return $this->trail;
    }

    /**
     * Generate the trail
     *
     * @return array
     */
    public function generate(): array {
        $page_types = [
            'page',
            'single',
            'category',
            'tag',
            '404',
        ];

        foreach ( $page_types as $page_type ) {
            if ( call_user_func( "is_{$page_type}" ) ) {
                $this->{"generate_trail_{$page_type}"}();
            }
        }

        $front_id = get_option( 'page_on_front' );
        $this->add_crumb( get_bloginfo( 'url' ), get_the_title( $front_id ), 'page', $front_id );
        $this->reverse();

        return $this->get_trail();
    }

    /**
     * Get the crumb items as a 2D-array.
     * Each item is an array with keys: title, url
     *
     * @return array
     */
    public static function get(): array {
        return self::new()->generate();
    }

    /**
     * Is the object in the menu map
     *
     * @param string $object_type
     * @param string $object_id
     *
     * @return boolean
     */
    private function is_object_in_menu( $object_type, $object_id ): bool {
        // if no menus exist
        if (count($this->menu_map) === 0) {
            return false;
        }
        
        return array_key_exists( "{$object_type}{$object_id}", $this->menu_map );
    }

    /**
     * Get the menu title of the object (page, post, term...) if it exists in a menu
     *
     * @param string $object_type
     * @param string $object_id
     *
     * @return string
     */
    private function get_object_menu_title( $object_type, $object_id ): string {
        if ( $this->is_object_in_menu( $object_type, $object_id ) ) {
            return $this->menu_map["{$object_type}{$object_id}"]->title;
        }

        return '';
    }

    /**
     * Add a crumb to the crumb array
     *
     * @param string $url
     * @param string $title
     * @param string $object_type
     * @param string $object_id
     *
     * @return void
     */
    private function add_crumb( $url, $title, $object_type = '', $object_id = '' ): void {
        $item = [
            'url'   => $url,
            'title' => $title,
        ];

        if ( $menu_title = $this->get_object_menu_title( $object_type, $object_id ) ) {
            $item['title'] = $menu_title;
        }

        $this->trail[] = $item;
    }

    /**
     * Reverse the trail array
     *
     * @return void
     */
    private function reverse(): void {
        $this->trail = array_reverse( $this->trail );
    }

    /**
     * Generate the trail for a page
     *
     * @return void
     */
    private function generate_trail_page(): void {
        global $post;

        $this->add_crumb( get_permalink(), get_the_title(), 'page', get_the_ID() );
        $curr_post = $post;

        while ( ( $curr_post_id = $curr_post->post_parent ) !== 0 ) {
            $curr_post = get_post( $curr_post_id );
            $this->add_crumb( get_permalink( $curr_post_id ), get_the_title( $curr_post_id ), 'page', $curr_post->ID );
        }
    }

    /**
     * Generate the trail for a post
     *
     * @return void
     */
    private function generate_trail_single(): void {
        // global $post;

        $this->add_crumb( get_permalink(), get_the_title() );

        if ( $category = current( get_the_category() ) ) {
            $this->add_crumb( get_term_link( $category ), $category->name, 'category', (string) $category->term_id );
            $this->term_ancestors( $category->term_id, 'category' );
        }
    }

    /**
     * Generate the trail for a category
     *
     * @return void
     */
    private function generate_trail_category(): void {
        $curr_category = get_category( get_queried_object() );

        $this->add_crumb( get_term_link( $curr_category ), single_cat_title( $prefix = '', $display = false ) );

        if ( $curr_category->parent !== 0 ) {
            $this->term_ancestors( $curr_category->term_id, 'category' );
        }
    }

    /**
     * Generate the trail for a tag
     *
     * @return void
     */
    private function generate_trail_tag(): void {
        $tag = get_queried_object();

        $this->add_crumb( get_term_link( $tag ), single_tag_title( $prefix = '', $display = false ) );
    }

    /**
     * Generate the trail for 404
     *
     * @return void
     */
    private function generate_trail_404(): void {
        $this->add_crumb( '', '404' );
    }

    /**
     * Generate the terms ancestors, eg. parent categories
     *
     * @param int $term_id
     * @param string $taxonomy
     *
     * @return void
     */
    private function term_ancestors( $term_id, $taxonomy ): void {
        $ancestors = get_ancestors( $term_id, $taxonomy );

        foreach ( $ancestors as $ancestor ) {
            $ancestor = get_term( $ancestor, $taxonomy );

            if ( $ancestor !== null && ! is_wp_error( $ancestor ) ) {
                $this->add_crumb( get_term_link( $ancestor ), $ancestor->name, $taxonomy, $ancestor->term_id );
            }
        }
    }
}
