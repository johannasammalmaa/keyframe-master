<?php

if ( ! function_exists( 'vd' ) ) {
    /**
     * Slightly better formatted var_dump
     */
    function vd() {
        echo '<pre>';
        foreach ( func_get_args() as $arg ) {
            var_dump( $arg );
        }
        echo '</pre>';
    }
}

if ( ! function_exists( 'dd' ) ) {
    /**
     * Dump & Die
     */
    function dd() {
        vd( func_get_args() );
        die();
    }
}

/**
 * Get array of ids for frontpage in a normalized way.
 * If Polylang is installed, get ids for all languages,
 * otherwise just get the option 'page_on_front'
 *
 * @return array
 */
function get_frontpage_ids() {
    if ( function_exists( 'pll_get_post_translations' ) ) {
        return pll_get_post_translations( get_option( 'page_on_front' ) );
    }

    return [ (int) get_option( 'page_on_front' ) ];
}

/**
 * @param int $id
 *
 * @return bool
 */
function is_frontpage( int $id ): bool {
    return in_array( $id, get_frontpage_ids(), false );
}

/**
 * Helper function for getting an array of crumbs.
 *
 * @return array
 */
function breadcrumbs(): array {
    return Breadcrumbs::get();
}
