<?php

/**
 * The core navigation file
 *
 * A custom WordPress nav walker class, using the WordPress built in menu manager.
 */
class Custom_Nav_Walker extends Walker_Nav_Menu {
    /**
     * @param string $output
     * @param int $depth
     * @param array $args
     */
    public function start_lvl( &$output, $depth = 0, $args = [] ): void {
        $output .= '<ul class="sub-menu">';
    }

    /**
     * @param string $output
     * @param \WP_Post $item
     * @param int $depth
     * @param array|stdClass $args
     * @param int $id
     */
    public function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ): void {
        $is_frontpage    = in_array( (int) $item->object_id, get_frontpage_ids(), false );
        $is_current_page = get_the_ID() === (int) $item->object_id;

        // $classes      = empty( $item->classes ) ? [] : (array) $item->classes;
        $classes   = [];
        $classes[] = $args->item_class;
        // $classes[] = 'menu-item-' . $item->ID;

        if ( $is_current_page ) {
            $classes[] = 'is-active';
        }

        if ( $is_frontpage ) {
            $classes[] = 'is-frontpage';
        }

        if ( $args->has_children ) {
            $classes[] = 'dropdown';
        }

        $class_names = implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id_tag = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args );
        // $id_tag = $id_tag ? ' id="' . esc_attr( $id_tag ) . '"' : '';
        $output .= '<li' . $class_names . '>';

        $atts           = [];
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target ) ? $item->target : '';
        $atts['rel']    = ! empty( $item->xfn ) ? $item->xfn : '';

        // If item has_children add atts to a.

        if ( $args->has_children && 0 === $depth ) {
            $atts['href']        = ! empty( $item->url ) ? $item->url : '';
            $atts['data-toggle'] = 'dropdown';
            $atts['class']       = 'dropdown';
        } else {
            $atts['href'] = ! empty( $item->url ) ? $item->url : '';
        }

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value      = ( $attr === 'href' ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args->before;

        $item_output .= '<a' . $attributes . '>';

        if ( $is_frontpage ) {
            $item_output .= '<img src="https://via.placeholder.com/250x250" alt="' . $item->title . '" />';
        } else {
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        }
        $item_output .= ( $args->has_children && 0 === $depth ) ? ' </a>' : '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    /**
     * Traverse elements to create list from elements.
     *
     * Display one element if the element doesn't have any children otherwise,
     * display the element and its children. Will only traverse up to the max
     * depth and no ignore elements under that depth.
     *
     * This method shouldn't be called directly, use the walk() method instead.
     *
     * @param object $element Data object.
     * @param array $children_elements List of elements to continue traversing.
     * @param int $max_depth Max depth to traverse.
     * @param int $depth Depth of current element.
     * @param array $args Args.
     * @param string $output Passed by reference. Used to append additional content.
     *
     * @see Walker::start_el()
     * @since 2.5.0
     *
     */
    public function display_element(
        $element,
        &$children_elements,
        $max_depth,
        $depth,
        $args,
        &$output
    ): void {
        if ( ! $element ) {
            return;
        }

        $id_field = $this->db_fields['id'];

        // Display this element.
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
        }

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

    /**
     * Menu Fallback
     * =============
     * If this function is assigned to the wp_nav_menu's fallback_cb variable
     * and a manu has not been assigned to the theme location in the WordPress
     * menu manager the function with display nothing to a non-logged in user,
     * and will add a link to the WordPress menu manager if logged in as an admin.
     *
     * @param array $args passed from the wp_nav_menu function.
     */
    public static function fallback( $args ): void {
        if ( current_user_can( 'manage_options' ) ) {

            extract( $args, null );

            $fb_output = null;

            if ( $container ) {
                $fb_output = '<' . $container;

                if ( $container_id ) {
                    $fb_output .= ' id="' . $container_id . '"';
                }

                if ( $container_class ) {
                    $fb_output .= ' class="' . $container_class . '"';
                }

                $fb_output .= '>';
            }

            $fb_output .= '<ul';

            if ( $menu_id ) {
                $fb_output .= ' id="' . $menu_id . '"';
            }

            if ( $menu_class ) {
                $fb_output .= ' class="' . $menu_class . '"';
            }

            $fb_output .= '>';
            $fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Add a menu</a></li>';
            $fb_output .= '</ul>';

            if ( $container ) {
                $fb_output .= '</' . $container . '>';
            }

            echo $fb_output;
        }
    }
}
