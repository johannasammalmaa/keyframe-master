<?php
/**
 * Register siderbars, widgets, etc.
 */

/**
 * Register sidebar
 *
 * @return void
 */
function creamedia_starter_sidebar_init() {
    register_sidebar( [
        'name'          => 'Sidebar 1',
        'id'            => 'sidebar_1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ] );
}
add_action( 'widgets_init', 'creamedia_starter_sidebar_init' );

/**
 * Unregister default widgets
 *
 * @return void
 */
function unregister_default_widgets() {
    unregister_widget( 'WP_Widget_Pages' );
    unregister_widget( 'WP_Widget_Calendar' );
    unregister_widget( 'WP_Widget_Archives' );
    unregister_widget( 'WP_Widget_Links' );
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Search' );
    unregister_widget( 'WP_Widget_Text' );
    unregister_widget( 'WP_Widget_Categories' );
    // unregister_widget( 'WP_Widget_Recent_Posts' );
    unregister_widget( 'WP_Widget_Recent_Comments' );
    unregister_widget( 'WP_Widget_RSS' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );
    unregister_widget( 'WP_Nav_Menu_Widget' );
    unregister_widget( 'WP_Widget_Media_Audio' );
    unregister_widget( 'WP_Widget_Media_Gallery' );
    unregister_widget( 'WP_Widget_Media_Image' );
    unregister_widget( 'WP_Widget_Media_Video' );
    unregister_widget( 'WP_Widget_Custom_HTML' );

    /* Polylang widgets */
    unregister_widget( 'PLL_Widget_Calendar' );
    unregister_widget( 'PLL_Widget_Languages' );
}
add_action( 'widgets_init', 'unregister_default_widgets', 11 );
