<?php
/**
 * Filter functions
 */

/**
 * Set the excert length
 */
add_filter( 'excerpt_length', function () {
    return 50;
} );

/**
 * Set the 'more' string for excert
 */
add_filter( 'excerpt_more', function () {
    return '&hellip;';
} );

/**
 * Make custom archive link
 *
 * @param string $link_html
 * @param string $url
 * @param string $text
 * @param string $format
 * @param string $before
 * @param string $after
 *
 * @return string
 */
function custom_archive_link(
    $link_html,
    $url,
    $text,
    $format,
    $before,
    $after
) {
    return <<<HTML
        <li class="mb-2">
            <p class="mb-0 text-lg">
                {$before}<a href="{$url}">{$text}</a>{$after}
            </p>
        </li>
HTML;
}
add_filter( 'get_archives_link', 'custom_archive_link', 10, 6 );
