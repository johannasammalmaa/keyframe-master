<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gutenbergtheme
 */
?>
<!doctype html>
<!--suppress HtmlRequiredLangAttribute -->
<html class="no-js" <?php language_attributes(); ?>>

<!--suppress HtmlRequiredTitleElement -->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,700;0,900;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--<header id="header" class="flex fixed w-full bg-transparent px-8 py-4 md:p-8 z-30 top-0 content-center items-center">-->
<header id="header" class="flex fixed w-full bg-transparent px-8 py-6 lg:px-0 z-30 top-0 content-center items-center h-auto">
<ul class="custom-languages hidden lg:absolute lg:pin-r lg:pin-t lg:block"><?php pll_the_languages( $args );?></ul>
        <div class="logo inline-flex w-1/2 md:w-3/4 lg:w-1/5 lg:ml-8 h-8 md:min-h-8 md:max-h-8">
            <!--<img src="<?php /*echo get_template_directory_uri();*/ ?>/images/Keyframe-logo-nega-white.svg" alt="Keyframe"/>-->
                <svg id="Logo_nega" data-name="Logo nega" xmlns="http://www.w3.org/2000/svg" class="w-full h-auto md:h-full md:w-auto align-middle" viewBox="0 0 386.347 47.917">
                <path class="logo-path" id="Path_1" data-name="Path 1" d="M240.86,58.047l-13.6-12.513L224.413,47.9v10.15H217.99V26.06h6.423V39.821L240.99,26.06H250.7L232.367,41.339l18.1,16.708Z" transform="translate(-120.084 -14.356)" fill="#fff"/>
                <path class="logo-path" id="Path_2" data-name="Path 2" d="M317.305,65.778a7.86,7.86,0,0,1-7.855-7.855V41.66a7.86,7.86,0,0,1,7.855-7.86h22.807v6.081h-21.85a2.282,2.282,0,0,0-2.394,2.389v3.984h20.152v6.081H315.868v4.985a2.282,2.282,0,0,0,2.394,2.389h21.85v6.068Z" transform="translate(-178.551 -22.101)" fill="#fff"/>
                <path class="logo-path" id="Path_3" data-name="Path 3" d="M377.989,58.047V44.56l-15.5-18.5H371l10.186,12.418L391.369,26.06h8.511l-15.495,18.5V58.047Z" transform="translate(-199.685 -14.356)" fill="#fff"/>
                <path class="logo-path" id="Path_4" data-name="Path 4" d="M466,65.778V41.66a7.86,7.86,0,0,1,7.855-7.86h22.807v6.081H474.794a2.277,2.277,0,0,0-2.389,2.389v3.984h20.148v6.081H472.4V65.81Z" transform="translate(-264.789 -22.101)" fill="#fff"/>
                <path class="logo-path" id="Path_5" data-name="Path 5" d="M568.964,65.77V54.91c0-.5,0-1.433-2.412-1.433h-16.29V65.77H543.84V33.792h23.8a9.329,9.329,0,0,1,5.515,1.554,5.67,5.67,0,0,1,2.367,4.909V45.2c0,2.214-.449,3.71-1.347,4.568a3.83,3.83,0,0,1-.579.449,5.1,5.1,0,0,1,.674.687,7.3,7.3,0,0,1,1.249,4.666v10.2Zm-2.412-18.387c2.412,0,2.412-.93,2.412-1.433v-4.94c0-.5,0-1.433-2.412-1.433h-16.29V47.4Z" transform="translate(-307.669 -22.092)" fill="#fff"/>
                <path class="logo-path" id="Path_6" data-name="Path 6" d="M650.055,65.778,645.4,56.7H629.786l-4.644,9.081H617.92L634.443,33.8h6.288l16.505,31.978Zm-7.846-15.136-4.63-9.032-4.64,9.032Z" transform="translate(-348.477 -22.101)" fill="#fff"/>
                <path class="logo-path" id="Path_7" data-name="Path 7" d="M725.161,58.047V39.983L714.067,57.337h-5.048L697.926,39.983V58.047h-6.4V26.06h5.237l14.776,23.4,14.776-23.4h5.237V58.047Z" transform="translate(-380.943 -14.356)" fill="#fff"/>
                <path class="logo-path" id="Path_8" data-name="Path 8" d="M817.8,65.778a7.86,7.86,0,0,1-7.855-7.855V41.66A7.86,7.86,0,0,1,817.8,33.8H840.6v6.081h-21.85a2.277,2.277,0,0,0-2.389,2.389v3.984h20.148v6.081H816.363v4.985a2.273,2.273,0,0,0,2.389,2.389H840.6v6.068Z" transform="translate(-454.255 -22.101)" fill="#fff"/>
                <path class="logo-path" id="Path_9" data-name="Path 9" d="M27.275,65.946A9.284,9.284,0,0,1,18,56.671V39.636a9.284,9.284,0,0,1,9.275-9.266h9.432L33.661,37.5H27.275a2.147,2.147,0,0,0-2.142,2.133V56.671a2.147,2.147,0,0,0,2.142,2.142h6.153l.121.076a11.5,11.5,0,0,0,1.581.822l13.025,5.524-1.388.377a9.216,9.216,0,0,1-2.448.332Z" transform="translate(-18 -20.211)" fill="#fff"/>
                <path class="logo-path" id="Path_10" data-name="Path 10" d="M84.833,59.033a9.692,9.692,0,0,1-3.8-.773L64.159,51.074A9.746,9.746,0,0,1,59,38.323l7.186-16.874a9.746,9.746,0,0,1,12.773-5.161l5.65,2.4-5.664,5.641-2.9-1.231a2.349,2.349,0,0,0-3.072,1.244L65.785,41.216a2.344,2.344,0,0,0,1.244,3.072l14.821,6.288,7.4,7.4-.687.287a9.755,9.755,0,0,1-3.728.772Z" transform="translate(-40.162 -12.027)" fill="#fff"/>
                <path class="logo-path" id="Path_11" data-name="Path 11" d="M125.818,55.668a10.137,10.137,0,0,1-7.186-2.978L104.839,38.911a10.2,10.2,0,0,1,0-14.4l13.775-13.779a10.2,10.2,0,0,1,14.408,0L146.8,24.508a10.2,10.2,0,0,1,0,14.4L133.022,52.686a10.114,10.114,0,0,1-7.2,2.982Zm0-40.256a2.5,2.5,0,0,0-1.8.737L110.26,29.924a2.524,2.524,0,0,0,0,3.566L124.035,47.27a2.529,2.529,0,0,0,3.566,0L141.38,33.49a2.524,2.524,0,0,0,0-3.566L127.6,16.149a2.488,2.488,0,0,0-1.783-.737Z" transform="translate(-64.197 -7.751)"  fill="#fff"/>
                <image src="<?php echo get_template_directory_uri();?>/images/Keyframe-logo-nega-white.svg" xlink:href="<?php echo esc_url( home_url( '/' ) ); ?>" />
                </svg>
                <a class="block w-full h-full absolute" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"></a>
        </div>
        <div class="inline-flex w-1/4 lg:hidden z-30 h-full content-center justify-end">
        <?php 
        $company_phone = carbon_get_theme_option( 'crb_company_phone' );
        $company_phone_strip = str_replace(' ', '', $company_phone);?>
            <a class="" href="tel:<?= $company_phone_strip ?>">
                <svg id="mobile-call" xmlns="http://www.w3.org/2000/svg" class="w-8 h-auto align-middle"  viewBox="0 0 29.98 30">
                    <g id="Group_18" data-name="Group 18">
                        <path id="Path_31" data-name="Path 31" d="M82,56.124q.083-.428.167-.856a7.056,7.056,0,0,1,2.486-4.027,2.456,2.456,0,0,1,3.309.121q2.212,2.176,4.373,4.4a2.471,2.471,0,0,1-.071,3.5,3.23,3.23,0,0,0-.958,1.44,3.06,3.06,0,0,0,.418,2.268,19.061,19.061,0,0,0,4.193,5.18,16.68,16.68,0,0,0,4,2.766,2.945,2.945,0,0,0,2.225.318,3.1,3.1,0,0,0,1.315-.891,2.339,2.339,0,0,1,1.775-.757,2.3,2.3,0,0,1,1.659.683c1.474,1.465,2.955,2.924,4.4,4.418a5.735,5.735,0,0,1,.689,1.163v1.171a5.023,5.023,0,0,1-.5.938,7,7,0,0,1-2.793,2.145c-.6.235-1.24.368-1.862.547h-1.7A.553.553,0,0,0,105,80.6a6.414,6.414,0,0,1-2.68-1.017A70.083,70.083,0,0,1,95.6,74.369,75.978,75.978,0,0,1,83.312,60.691a6.847,6.847,0,0,1-1.046-2.173c-.112-.421-.178-.854-.265-1.282Z" transform="translate(-82 -50.659)" fill="#fff"/>
                    </g>
                </svg>
            </a>
        </div>
        <div class="inline-flex w-1/4 lg:hidden justify-end z-40 h-full">
            <div data-menu-toggle class="menu-container" onclick="myFunction(this)">
                <div class="bar bar1" id="bar1"></div>
                <div class="bar bar2" id="bar2"></div>
                <div class="bar bar3" id="bar3"></div>
             </div>
        </div>

            <script>
                function myFunction(x) {
                x.classList.toggle("change");
                }
            </script>

            <!-- Nav -->
            <nav class="main-navigation w-full bg-pink lg:bg-transparent lg:w-4/5 flex lg:inline-flex" id="primary-navigation">
                
                <?php
                wp_nav_menu( [
                    'theme_location' => 'header-menu',
                    'container'      => false,
                    'depth'          => 2,
                    'menu_id'        => 'primary-menu',
                    'menu_class'     => 'site-navigation flex',
                    'items_wrap'     => '<ul class="%2$s" id="%1$s">%3$s</ul>',
                    'after'          => '<span id="sub-menu-caret" class="js-menu-caret sub-menu-caret text-white "><svg id="submenu_indicator" data-name="submenu indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.759 6.654">
                    <path id="submenu_indicator-2" data-name="submenu indicator" d="M17.785,6.653H14.064c.048-.061.1-.123.152-.182L18.962,1.15a3.253,3.253,0,0,1,4.964,0l4.747,5.321c.052.058.151.18.151.18l-3.722,0L22.058,3.243a.808.808,0,0,0-.611-.285h-.011a.823.823,0,0,0-.611.285l-3.04,3.409Z" transform="translate(28.823 6.653) rotate(180)" fill="#fff"/>
                  </svg></span>',
                ] );
                ?>

              

            </nav>
        <!-- ./Nav -->
</header>
