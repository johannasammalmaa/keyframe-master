<?php get_header(); ?>

<main class="container container--base py-8">
    <div class="flex">
        <div class="w-2/3 px-2">
            <h1>Posts</h1>

            <?php while ( have_posts() ): the_post(); ?>
                <article>
                    <time>
                        <small><?= get_the_date() ?></small>
                    </time>

                    <h2>
                        <?php the_title() ?>
                    </h2>

                    <p>
                        <?= wp_trim_excerpt( get_the_excerpt() ); ?>
                    </p>

                    <p>
                        <a href="<?php the_permalink() ?>">Lue lisää</a>
                    </p>
                </article>
            <?php endwhile; ?>
        </div>

        <div class="w-1/3 px-2">
            <?php if ( is_active_sidebar( 'sidebar_1' ) ): ?>
                <div class="sidebar-widget">
                    <?php dynamic_sidebar( 'sidebar_1' ) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>
