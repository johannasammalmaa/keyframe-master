<?php get_header(); ?>
<main class="container--full relative nf-hack pt-24 bg-pink">
            <div class="w-full flex flex-wrap h-screen -mt-2 content-center">
                <?php while ( have_posts() ): the_post();
                    the_content();
                endwhile;
                $heading= carbon_get_theme_option( 'crb_404_heading' );
                $text= carbon_get_theme_option( 'crb_404_text' );
                ?>
                <?php if ( ! empty( $heading ) ): ?>
                <div class="w-full text-center font-montserrat text-15xl font-black text-black uppercase"><?= $heading ?></div>
                <?php endif; ?>
                <?php if ( ! empty( $text ) ): ?>
                <div class="w-full text-center mt-4"><?= $text?></div>
                <?php endif; ?>
            </div>
</main>
<?php get_template_part( 'template-parts/contact-form')?>
<?php get_footer(); ?>