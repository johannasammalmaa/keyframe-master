<?php get_header(); ?>

<?php get_template_part( 'template-parts/hero', get_post_type() ); ?>

<main class="container container--base py-12">
    <div class="flex">
        <div class="w-2/3 px-2">
            <?php while ( have_posts() ): the_post();
                the_content();
            endwhile; ?>
        </div>

        <div class="w-1/3 px-2">
            <?php if ( is_active_sidebar( 'sidebar_1' ) ): ?>
                <div class="sidebar-widget">
                    <?php dynamic_sidebar( 'sidebar_1' ) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>
