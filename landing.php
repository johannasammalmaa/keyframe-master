<?php
/**
* Template Name: Landing page
*/?>
<!--suppress HtmlRequiredTitleElement -->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,700;0,900;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header id="header" class="flex fixed w-full bg-transparent px-8 py-6 lg:px-0 z-30 top-0 content-center items-center h-auto">
        <div class="logo inline-flex w-1/2 md:w-3/4 lg:w-1/5 lg:ml-8 h-8 md:min-h-8 md:max-h-8">
            <!--<img src="<?php /*echo get_template_directory_uri();*/ ?>/images/Keyframe-logo-nega-white.svg" alt="Keyframe"/>-->
                <svg id="Logo_nega" data-name="Logo nega" xmlns="http://www.w3.org/2000/svg" class="w-full h-auto md:h-full md:w-auto align-middle" viewBox="0 0 386.347 47.917">
                <path class="logo-path" id="Path_1" data-name="Path 1" d="M240.86,58.047l-13.6-12.513L224.413,47.9v10.15H217.99V26.06h6.423V39.821L240.99,26.06H250.7L232.367,41.339l18.1,16.708Z" transform="translate(-120.084 -14.356)" fill="#fff"/>
                <path class="logo-path" id="Path_2" data-name="Path 2" d="M317.305,65.778a7.86,7.86,0,0,1-7.855-7.855V41.66a7.86,7.86,0,0,1,7.855-7.86h22.807v6.081h-21.85a2.282,2.282,0,0,0-2.394,2.389v3.984h20.152v6.081H315.868v4.985a2.282,2.282,0,0,0,2.394,2.389h21.85v6.068Z" transform="translate(-178.551 -22.101)" fill="#fff"/>
                <path class="logo-path" id="Path_3" data-name="Path 3" d="M377.989,58.047V44.56l-15.5-18.5H371l10.186,12.418L391.369,26.06h8.511l-15.495,18.5V58.047Z" transform="translate(-199.685 -14.356)" fill="#fff"/>
                <path class="logo-path" id="Path_4" data-name="Path 4" d="M466,65.778V41.66a7.86,7.86,0,0,1,7.855-7.86h22.807v6.081H474.794a2.277,2.277,0,0,0-2.389,2.389v3.984h20.148v6.081H472.4V65.81Z" transform="translate(-264.789 -22.101)" fill="#fff"/>
                <path class="logo-path" id="Path_5" data-name="Path 5" d="M568.964,65.77V54.91c0-.5,0-1.433-2.412-1.433h-16.29V65.77H543.84V33.792h23.8a9.329,9.329,0,0,1,5.515,1.554,5.67,5.67,0,0,1,2.367,4.909V45.2c0,2.214-.449,3.71-1.347,4.568a3.83,3.83,0,0,1-.579.449,5.1,5.1,0,0,1,.674.687,7.3,7.3,0,0,1,1.249,4.666v10.2Zm-2.412-18.387c2.412,0,2.412-.93,2.412-1.433v-4.94c0-.5,0-1.433-2.412-1.433h-16.29V47.4Z" transform="translate(-307.669 -22.092)" fill="#fff"/>
                <path class="logo-path" id="Path_6" data-name="Path 6" d="M650.055,65.778,645.4,56.7H629.786l-4.644,9.081H617.92L634.443,33.8h6.288l16.505,31.978Zm-7.846-15.136-4.63-9.032-4.64,9.032Z" transform="translate(-348.477 -22.101)" fill="#fff"/>
                <path class="logo-path" id="Path_7" data-name="Path 7" d="M725.161,58.047V39.983L714.067,57.337h-5.048L697.926,39.983V58.047h-6.4V26.06h5.237l14.776,23.4,14.776-23.4h5.237V58.047Z" transform="translate(-380.943 -14.356)" fill="#fff"/>
                <path class="logo-path" id="Path_8" data-name="Path 8" d="M817.8,65.778a7.86,7.86,0,0,1-7.855-7.855V41.66A7.86,7.86,0,0,1,817.8,33.8H840.6v6.081h-21.85a2.277,2.277,0,0,0-2.389,2.389v3.984h20.148v6.081H816.363v4.985a2.273,2.273,0,0,0,2.389,2.389H840.6v6.068Z" transform="translate(-454.255 -22.101)" fill="#fff"/>
                <path class="logo-path" id="Path_9" data-name="Path 9" d="M27.275,65.946A9.284,9.284,0,0,1,18,56.671V39.636a9.284,9.284,0,0,1,9.275-9.266h9.432L33.661,37.5H27.275a2.147,2.147,0,0,0-2.142,2.133V56.671a2.147,2.147,0,0,0,2.142,2.142h6.153l.121.076a11.5,11.5,0,0,0,1.581.822l13.025,5.524-1.388.377a9.216,9.216,0,0,1-2.448.332Z" transform="translate(-18 -20.211)" fill="#fff"/>
                <path class="logo-path" id="Path_10" data-name="Path 10" d="M84.833,59.033a9.692,9.692,0,0,1-3.8-.773L64.159,51.074A9.746,9.746,0,0,1,59,38.323l7.186-16.874a9.746,9.746,0,0,1,12.773-5.161l5.65,2.4-5.664,5.641-2.9-1.231a2.349,2.349,0,0,0-3.072,1.244L65.785,41.216a2.344,2.344,0,0,0,1.244,3.072l14.821,6.288,7.4,7.4-.687.287a9.755,9.755,0,0,1-3.728.772Z" transform="translate(-40.162 -12.027)" fill="#fff"/>
                <path class="logo-path" id="Path_11" data-name="Path 11" d="M125.818,55.668a10.137,10.137,0,0,1-7.186-2.978L104.839,38.911a10.2,10.2,0,0,1,0-14.4l13.775-13.779a10.2,10.2,0,0,1,14.408,0L146.8,24.508a10.2,10.2,0,0,1,0,14.4L133.022,52.686a10.114,10.114,0,0,1-7.2,2.982Zm0-40.256a2.5,2.5,0,0,0-1.8.737L110.26,29.924a2.524,2.524,0,0,0,0,3.566L124.035,47.27a2.529,2.529,0,0,0,3.566,0L141.38,33.49a2.524,2.524,0,0,0,0-3.566L127.6,16.149a2.488,2.488,0,0,0-1.783-.737Z" transform="translate(-64.197 -7.751)"  fill="#fff"/>
                <image src="<?php echo get_template_directory_uri();?>/images/Keyframe-logo-nega-white.svg" xlink:href="<?php echo esc_url( home_url( '/' ) ); ?>" />
                </svg>
</header>
<main class="container--full relative nf-hack">
        <div class="flex flex-wrap">
            <div class="w-full -mt-2">
                <?php while ( have_posts() ): the_post();
                    the_content();
                endwhile; ?>
            </div>
         </div>
</main>
<?php get_footer(); ?>
