<?php

/**
 * URL to RSS Feed
 */
$feed_url = '';

/**
 * How many items to show
 */
$feed_amount_to_show = 5;

try {
    if ( file_exists( ABSPATH . WPINC . '/class-simplepie.php' ) ) {
        require_once ABSPATH . WPINC . '/class-simplepie.php';
    } else {
        throw new RuntimeException( 'SimplePie class unavailable' );
    }

    /** @var \SimplePie $simplepie */
    $simplepie = new SimplePie();

    $simplepie->set_feed_url( $feed_url );
    $simplepie->set_cache_location( WP_CONTENT_DIR . '/uploads' );
    $simplepie->init();
    $simplepie->enable_order_by_date( false );

    /** @var array $items */
    $feed_items = $simplepie->get_items();
} catch ( Exception $e ) {
    $feed_items = [];
}

$feed_items = array_slice( $feed_items, 0, $feed_amount_to_show );
?>

<h2 class="text-2xl font-noto-sans text-grey uppercase font-bold tracking-tight">
    RSS Feed title
</h2>
<?php if ( is_array( $feed_items ) && count( $feed_items ) > 0 ): ?>
    <div class="rss-feed">
        <ul>
            <?php foreach ( $feed_items as $item ): ?>
                <?php
                /** @var \SimplePie_Item $item */
                try {
                    $date = ( new DateTime( $item->get_date() ) )->format( 'j.n.Y' );
                } catch ( Exception $e ) {
                    $date = null;
                } ?>
                <li>
                    <time><?= $date ?></time>
                    <a href="<?= $item->get_link() ?>" target="_blank" rel="noopener">
                        <?= $item->get_title() ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
