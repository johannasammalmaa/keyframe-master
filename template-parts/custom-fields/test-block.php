<?php

/**
 * @type array $args
 */

?>

<section class="custom-test-block">
    <h2><?= $args['name'] ?></h2>
    <?= $args['description'] ?>
    <img src="<?= wp_get_attachment_image_src( $args['image'] )[0] ?>" alt="" />
</section>

