<?php
/** @var array $logos */
$logos = carbon_get_theme_option( 'crb_logos' );
?>

<?php if ( is_array( $logos ) && count( $logos ) > 0 ): ?>
    <div class="carousel relative py-8 js-carousel">
        <div class="carousel__frame js-carousel-frame">
            <ul class="carousel__slides js-carousel-slides">
                <?php foreach ( $logos as $logo ): ?>
                    <?php if ( empty( $logo['image'] ) ) : continue; endif; ?>
                    <li class="carousel__slide">
                        <img src="<?= wp_get_attachment_image_src( $logo['image'], 'carousel-logo' )[0] ?>"
                             alt="<?= $logo[ 'title' . crb_get_i18n_suffix() ] ?>" />
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <!--<div class="carousel__button carousel__button--prev js-carousel-prev">
            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5">
                <path fill="#2E435A" d="M302.7 90.9l55.7 55.5-103.8 104.3 103.8 104.4-55.7 55.5-159.1-159.9z"></path>
            </svg>
        </div>
        <div class="carousel__button carousel__button--next js-carousel-next">
            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5">
                <path fill="#2E435A" d="M199.3 410.6l-55.7-55.5 103.8-104.4-103.8-104.3 55.7-55.5 159.1 159.8z"></path>
            </svg>
        </div>-->
    </div>
<?php endif; ?>
