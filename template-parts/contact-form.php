<?php
$contact_forms = carbon_get_the_post_meta( 'crb_contact_form' );
$use_form = carbon_get_the_post_meta( 'use_form' );
$contact_form_img = carbon_get_the_post_meta( 'use_image' );

if ($use_form == 1 && is_array( $contact_forms ) && count( $contact_forms ) > 0 ):
?>
    <div class="contact-form flex flex-wrap flex-col lg:flex-row nf-hack">
        <?php foreach ( $contact_forms as $contact_form ): 
                $contact_form_img = $contact_form['use_image'];
                $bg_color = $contact_form['bg'];
                if ( $bg_color == "white" ) {
                    $form_class = 'form-wrapper--dark';
                } else {
                    $form_class = 'form-wrapper--light';
                };?>
                <div class="form-wrapper flex p-8 w-full lg:w-1/2 flex-grow flex-col <?= $form_class?> bg-<?= $bg_color?>">
                    <?php 
                    if ( ! empty($contact_form['form_title'] ) ) {?>
                    <h2 class="h6 w-full max-w-sm mx-auto"><?php echo $contact_form['form_title'];?></h2>
                    <?php }
                    if ( ! empty( $contact_form['ninja_shortcode'] ) ) {
                    $ninja_shortcode = $contact_form['ninja_shortcode'];
                    Ninja_Forms()->display($ninja_shortcode);
                    }
                    ?>
                    
                </div>
                <?php if ( $contact_form_img == 1 ) {?>
                <div class="image-wrapper w-full lg:w-1/2 flex min-h-60 p-4 p-12 items-end" style="background-image:url('<?=$contact_form['form_image']?>');">
                <?if ( ! empty( $contact_form['slogan'] ) ) ?>
                    <div class="h3 text-italic text-white font-bold italic">"<?php echo  $contact_form['slogan'];?>"</div>
                    }
                </div>
                <?php  } ?>
        <?php endforeach; ?>
           
    </div>
<?php endif; ?>