<?php
/** @var array $highlights */
$highlights = carbon_get_the_post_meta( 'crb_page_highlights' );

if ( is_front_page() ) {
    $highlights_class = 'highlights--front';
} else {
    $highlights_class = 'highlights--page';
}
?>
<?php if ( is_array( $highlights ) && count( $highlights ) > 0 ): ?>
    <div class="highlights <?= $highlights_class ?>">
        <?php foreach ( $highlights as $highlight ): ?>
            <?php
            $image_id        = null;
            $highlight_image = null;
            if ( ! empty( $highlight['image'] ) ) {
                $image_id = $highlight['image'];
            } elseif ( ! empty( get_post_thumbnail_id( $highlight['association'][0]['id'] ) ) ) {
                $image_id = get_post_thumbnail_id( $highlight['association'][0]['id'] );
            }

            if ( $image_id ) {
                $highlight_image = wp_get_attachment_image_src( $image_id, 'hero-large' )[0];
            }
            ?>
            <div class="highlights__item">
                <a class="highlights__link"
                   href="<?= get_permalink( $highlight['association'][0]['id'] ) ?>"
                   style="background-image:url('<?= $highlight_image ?>');">
                    <div class="highlights__content">
                        <p class="z-10 h3">
                            <?= $highlight[ 'title' . crb_get_i18n_suffix() ] ?>
                        </p>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
