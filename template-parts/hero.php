<?php
// Block settings
if ( is_front_page() ) {
    $hero_class = 'hero--front';
} else {
    $hero_class = 'hero--page';
}

// Featured image
if ( has_post_thumbnail() ) {
    $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'hero-large' )[0];
} else {
    $featured_image = wp_get_attachment_image_src( carbon_get_theme_option('crb_company_default_image'), 'full' )[0];
}


$text = carbon_get_the_post_meta( 'hero_text' );
$heading = carbon_get_the_post_meta( 'hero_heading' );


if ( carbon_get_the_post_meta( 'hero_video' ) ) {
    $video_id = carbon_get_the_post_meta( 'hero_video' );
}
?>

<style>
    .hero {
        background-image: url('<?php echo esc_url( $featured_image ); ?>');
    }
</style>

<div class="hero <?php echo $hero_class; ?> z-0 top-0 has-overlay">
<?php if (is_front_page()):?>
    <div class="hero-wrapper w-screen flex flex-column items-end h-screen">
        <div class="vimeo-wrapper hidden lg:block">
            <iframe class="w-full h-screen" src="https://player.vimeo.com/video/<?php echo $video_id?>?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" allow="autoplay; fullscreen"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <div class="hero__banner z-20 align-bottom pl-8 max-w-screen pb-16">
            <?php if ( ! empty( $heading ) ): ?>
                <h1 class="text-white uppercase text-left max-w-screen lg:max-w-lg xl:max-w-xl pr-8"> <?= $heading ?></h1>
            <?php endif; ?>
            <?php if ( ! empty( $text ) ): ?>
                 <p class="text-left pr-8 max-w-screen"><?= $text ?></p>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<?php if ( ! is_front_page() ): ?>
    <div class="hero-wrapper flex flex-column items-center w-screen h-full">
        <?php if ( ! empty( $video_id ) ): ?>
            <div class="vimeo-wrapper hidden lg:block">
                <iframe class="w-full h-full" src="https://player.vimeo.com/video/<?php echo $video_id?>?background=1&autoplay=1&loop=1&byline=0&title=0" frameborder="0" allow="autoplay; fullscreen"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
             </div>
        <?php endif; ?>
        <div class="hero__banner z-20 align-middle mx-auto max-w-screen">
            <h1 class="text-white mt-4 uppercase px-2"><?php single_post_title() ?></h1>
            <?php if ( ! empty( $text ) ): ?>
                <div class="mb-4">
                    <?= $text ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
 <?php endif; ?>
</div>