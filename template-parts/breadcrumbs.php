<?php
/** @var array $breadcrumbs */
$breadcrumbs = breadcrumbs();

$i     = 1;
$count = count( $breadcrumbs );
?>

<?php if ( is_array( $breadcrumbs ) && $count > 0 ): ?>
    <div class="bg-black py-3">
        <div class="container container--base">
            <nav>
                <ul class="breadcrumbs">
                    <?php foreach ( $breadcrumbs as $crumb ): ?>
                        <li class="breadcrumbs__item">
                            <?php if ( $crumb['url'] ): ?>
                                <a class="breadcrumbs__link <?= $count === $i ? 'active' : '' ?>"
                                   href="<?= $crumb['url'] ?>">
                                    <?= $crumb['title'] ?>
                                </a>
                            <?php else: ?>
                                <?= $crumb['title'] ?>
                            <?php endif; ?>
                        </li>

                        <?php $i ++; ?>
                    <?php endforeach; ?>
                </ul>
            </nav>
        </div>
    </div>
<?php endif; ?>
